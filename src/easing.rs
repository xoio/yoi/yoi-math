
// from https://github.com/spite/codevember-2021/blob/main/modules/easings.js
fn quad_in(t:f32)->f32{
    t * t
}

fn quad_out(t:f32)->f32{
    t * ( 2 - t)
}

