use glam::{Vec2, Vec3};

/// some functions ported from here https://github.com/spite/codevember-2021/blob/main/10/main.js#L72
///
pub fn rot2d(v: Vec2, a: f32) -> [f32; 2] {
    let c = a.cos();
    let s = a.sin();

    [v.x * c - v.y * s, v.x * s + v.y * c]
}

#[allow(dead_code)]
fn sd_circle(p: Vec2, r: f32) -> f32 {
    p.length() - r
}

pub fn sd_box_2d(p: Vec2) -> f32 {
    let p_max = p.x.max(p.y);
    p.length() + (p_max.min(0.0))
}

pub fn displacement(p: Vec3) -> f32 {
    p.x.sin() * p.y.sin() * p.z.sin()
}


pub fn mix(x: f32, y: f32, a: f32) -> f32 {
    if a <= 0.0 {
        return x;
    }

    if a >= 1.0 {
        return y;
    }

    x + a * (y - x)
}