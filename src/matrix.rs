use glam::{Mat4, Vec3A};

/// Wrapper around Mat4 to make some things easier.
pub trait Matrix {
    /// translates matrix.
    fn translate(&mut self, vec: Vec3A);

    /// rotates matrix.
    fn rotate(&mut self, rad: f32, axis: [f32; 3]);

    fn identity(&mut self);

    fn get_raw(&mut self) -> Mat4;
}

/// wrapper around Glam Mat4
#[derive(Copy, Clone)]
pub struct Matrix4 {
    matrix: Mat4,
}

impl Matrix4 {
    fn new() -> Matrix4 {
        Matrix4 {
            matrix: Mat4::IDENTITY
        }
    }

    #[allow(non_snake_case)]
    pub fn IDENTITY() -> Matrix4 {
        Matrix4::new()
    }

    #[allow(unused_must_use)]
    pub fn invert(self) -> Self {
        self.matrix.inverse();
        self
    }
}

impl Matrix for Matrix4 {
    fn get_raw(&mut self) -> Mat4 {
        self.matrix
    }

    fn identity(&mut self) {
        self.matrix = Mat4::IDENTITY;
    }


    /// translates the matrix.
    fn translate(&mut self, vec: Vec3A) {

        // x is reversed for some reason, flip it to follow OpenGL conventions.
        let mut v = vec;
        v.x = v.x * -1.0;

        self.matrix *= Mat4::from_translation(v.into());
    }

    /// rotates the matrix. ported from glMatrix.
    fn rotate(&mut self, rad: f32, axis: [f32; 3]) {
        let mut x = axis[0];
        let mut y = axis[1];
        let mut z = axis[2];

        let a = self.matrix.to_cols_array();

        #[allow(unused_parens)]
        let tmp_len = ((x * x) + (y * y) + (z * z));
        let mut len = (tmp_len as f32).sqrt();
        len = 1.0 / len;


        x *= len;
        y *= len;
        z *= len;

        let s = rad.sin();
        let c = rad.cos();
        let t = 1.0 - c;

        let a00 = a[0];
        let a01 = a[1];
        let a02 = a[2];
        let a03 = a[3];

        let a10 = a[4];
        let a11 = a[5];
        let a12 = a[6];
        let a13 = a[7];

        let a20 = a[8];
        let a21 = a[9];
        let a22 = a[10];
        let a23 = a[11];

        let b00 = x * x * t + c;
        let b01 = y * x * t + z * s;
        let b02 = z * x * t - y * s;
        let b10 = x * y * t - z * s;
        let b11 = y * y * t + c;
        let b12 = z * y * t + x * s;
        let b20 = x * z * t + y * s;
        let b21 = y * z * t - x * s;
        let b22 = z * z * t + c;

        let mut out = Mat4::IDENTITY.to_cols_array();

        out[0] = a00 * b00 + a10 * b01 + a20 * b02;

        out[1] = a01 * b00 + a11 * b01 + a21 * b02;

        out[2] = a02 * b00 + a12 * b01 + a22 * b02;

        out[3] = a03 * b00 + a13 * b01 + a23 * b02;

        out[4] = a00 * b10 + a10 * b11 + a20 * b12;

        out[5] = a01 * b10 + a11 * b11 + a21 * b12;

        out[6] = a02 * b10 + a12 * b11 + a22 * b12;

        out[7] = a03 * b10 + a13 * b11 + a23 * b12;

        out[8] = a00 * b20 + a10 * b21 + a20 * b22;

        out[9] = a01 * b20 + a11 * b21 + a21 * b22;

        out[10] = a02 * b20 + a12 * b21 + a22 * b22;

        out[11] = a03 * b20 + a13 * b21 + a23 * b22;

        self.matrix = Mat4::from_cols_array(&out);
    }
}

