use glam::Vec4;

/// Partially based on the following
/// https://github.com/Hi-Rez/Satin/blob/master/Sources/SatinCore/Bounds.mm
pub struct AABB {
    pub min: [f32; 3],
    pub max: [f32; 3],
}

impl AABB {
    pub fn new() -> Self {
        AABB {
            min: [f32::INFINITY, f32::INFINITY, f32::INFINITY],
            max: [-f32::INFINITY, -f32::INFINITY, -f32::INFINITY],
        }
    }

    pub fn create(min: [f32; 3], max: [f32; 3]) -> Self {
        AABB {
            min,
            max,
        }
    }

    pub fn set(mut self, min: [f32; 3], max: [f32; 3]) -> Self {
        self.min = min;
        self.max = max;

        self
    }

    /// Checks to see if the min values are actually numbers. If they aren't then the AABB has
    /// invalid values which makes it un-usable / faulty.
    pub fn is_nan(self) -> bool {
        if self.min[0].is_nan() || self.min[1].is_nan() || self.min[2].is_nan() {
            return true;
        }

        false
    }

    pub fn reset(&mut self) {
        self.min = [f32::INFINITY, f32::INFINITY, f32::INFINITY];
        self.max = [-f32::INFINITY, -f32::INFINITY, -f32::INFINITY]
    }

    pub fn set_from_attribute(&mut self, position: Vec<Vec4>) {
        let count = position.len();

        let mut verts = [0.0, 0.0, 0.0];
        for i in 0..count {
            verts[0] = position[i].x;
            verts[1] = position[i].y;
            verts[2] = position[i].z;
            self.expand_by_point(verts)
        }
    }

    pub fn expand_by_point(&mut self, point: [f32; 3]) {
        self.min[0] = f32::min(self.min[0], point[0]);
        self.min[1] = f32::min(self.min[1], point[1]);
        self.min[2] = f32::min(self.min[2], point[2]);

        self.max[0] = f32::max(self.max[0], point[0]);
        self.max[1] = f32::max(self.max[1], point[1]);
        self.max[2] = f32::max(self.max[2], point[2]);
    }

    pub fn contains_point(self, point: [f32; 3]) -> bool {
        return !(point[0] < self.min[0] || point[0] > self.max[0] ||
            point[1] < self.min[1] || point[1] > self.max[1] ||
            point[2] < self.min[2] || point[2] > self.max[2]);
    }

    pub fn is_empty(&mut self) -> bool {
        // this is a more robust check for empty than ( volume <= 0 ) because volume can get positive with two negative axes

        return (self.max[0] < self.min[0]) || (self.max[1] < self.min[1]) || (self.max[2] < self.min[2]);
    }

    pub fn get_center(&mut self, target: &mut [f32; 3]) {
        if self.is_empty() {
            target[0] = 0.0;
            target[1] = 0.0;
            target[2] = 0.0;
        } else {
            target[0] = self.min[0] + self.max[0];
            target[1] = self.min[1] + self.max[1];
            target[2] = self.min[2] + self.max[2];
        }
    }

    pub fn compute_volume(self) -> f32 {
        let mut vol = 1.0;
        for i in 0..3 {
            vol *= (self.max[i] - self.min[i])
        }
        vol
    }
}