use glam::{Vec3, vec3};
use crate::aabb::AABB;
use crate::core::create_vec3;


const _VECTOR: Vec3 = Vec3::new(0.0, 0.0, 0.0);
const _SEG_CENTER: Vec3 = Vec3::new(0.0, 0.0, 0.0);

const _SEG_DIR: Vec3 = Vec3::new(0.0, 0.0, 0.0);
const _DIFF: Vec3 = Vec3::new(0.0, 0.0, 0.0);


/// Based on the Three.js Ray class
/// https://github.com/mrdoob/three.js/blob/master/src/math/Ray.js
pub struct Ray {
    origin: Vec3,
    direction: Vec3,
}

impl Ray {
    pub fn new() -> Self {
        Ray {
            origin: create_vec3(),
            direction: create_vec3(),
        }
    }

    pub fn set(&mut self, origin: Vec3, direction: Vec3) {
        self.origin = origin;
        self.direction = direction;
    }

    pub fn at(&self, t: f32, target: Vec3) -> Vec3 {
        let mut target = Vec3::from(target);
        target.x += self.direction.x * t;
        target.y += self.direction.y * t;
        target.z += self.direction.z * t;

        return target;
    }

    pub fn look_at(&mut self, v: Vec3) {
        self.direction = Vec3::from(v);
        self.direction.x -= self.origin.x;
        self.direction.y -= self.origin.y;
        self.direction.z -= self.origin.z;
    }

    pub fn copy(&mut self, ray: Ray) {
        self.origin = Vec3::from(ray.origin);
        self.direction = Vec3::from(ray.direction);
    }

    pub fn recast(&mut self, t: f32) {
        let v = self.at(t, _VECTOR);
        self.origin = Vec3::from(v);

    }

    pub fn closest_pt_to_pt(self, point: Vec3, target: &mut Vec3) -> Vec3 {
        target.x = point.x - target.x;
        target.y = point.y - target.y;
        target.z = point.z - target.z;

        let direction_distance = target.dot(self.direction);

        if direction_distance < 0.0 {
            return Vec3::from(self.origin);
        }

        let mut t = Vec3::from(self.origin);
        t = self.direction * direction_distance;
        t
    }

    pub fn distance_to_pt(self, pt: Vec3) -> f32 {
        self.distance_to_pt(pt).sqrt()
    }

    pub fn distance_sq_to_pt(self, pt: Vec3) -> f32 {
        _VECTOR.x = pt.x - self.origin.x;
        _VECTOR.y = pt.y - self.origin.y;
        _VECTOR.z = pt.z - self.origin.z;

        let direction_distance = _VECTOR.dot(self.direction);

        if direction_distance < 0.0 {
            let x = pt.x - self.origin.x;
            let y = pt.y - self.origin.y;
            let z = pt.z - self.origin.z;
            return x * x + y * y + z * z;
        }

        let mut tmp_v = Vec3::from(self.origin);
        tmp_v += self.direction * direction_distance;

        _VECTOR.x = tmp_v.x;
        _VECTOR.y = tmp_v.y;
        _VECTOR.z = tmp_v.z;

        let x = pt.x - _VECTOR.x;
        let y = pt.y - _VECTOR.y;
        let z = pt.z - _VECTOR.z;
        return x * x + y * y + z * z;
    }

    pub fn distance_to_sq(self, i: Vec3, o: Vec3) -> f32 {
        let dx = i.x - o.x;
        let dy = i.y - o.y;
        let dz = i.z - o.z;
        return dx * dx + dy * dy + dz * dz;
    }

    pub fn intersect_box(self, bbox: AABB) -> bool {
        let b = self.intersects_box(bbox, _VECTOR);
        if b.x == 0.0 && b.y == 0.0 && b.z == 0.0 {
            return false;
        }
        return true;
    }

    pub fn intersects_box(self, bbox: AABB, target: Vec3) -> Vec3 {
        let mut tmin = 0.0;
        let mut tmax = 0.0;

        let mut tymin = 0.0;
        let mut tymax = 0.0;

        let mut tzmin = 0.0;
        let mut tzmax = 0.0;


        let invdirx = 1.0 / self.direction[0];
        let invdiry = 1.0 / self.direction[1];
        let invdirz = 1.0 / self.direction[2];

        if invdirx >= 0.0 {
            tmin = (bbox.min[0] - self.origin[0]) * invdirx;
            tmax = (bbox.max[0] - self.origin[0]) * invdirx;
        } else {
            tmin = (bbox.max[0] - self.origin[0]) * invdirx;
            tmax = (bbox.min[0] - self.origin[0]) * invdirx;


            if invdiry >= 0.0 {
                tymin = (bbox.min[1] - self.origin[1]) * invdiry;
                tymax = (bbox.max[1] - self.origin[1]) * invdiry;
            } else {
                tymin = (bbox.max[1] - self.origin[1]) * invdiry;
                tymax = (bbox.min[1] - self.origin[1]) * invdiry;
            }
        }

        if ((tmin > tymax) || (tymin > tmax)) {
            return vec3(-1.0, -1.0, -1.0); // treat -1 as null
        }
        if (tymin > tmin || f32::is_nan(tmin)) {
            tmin = tymin;
        }

        if (tymax < tmax || f32::is_nan(tmax)) {
            tmax = tymax;
        }

        if (invdirz >= 0.0) {
            tzmin = (bbox.min[2] - self.origin[2]) * invdirz;
            tzmax = (bbox.max[2] - self.origin[2]) * invdirz;
        } else {
            tzmin = (bbox.max[2] - self.origin[2]) * invdirz;
            tzmax = (bbox.min[2] - self.origin[2]) * invdirz;
        }

        if ((tmin > tzmax) || (tzmin > tmax)) {
            return create_vec3();
        }

        if (tzmin > tmin || tmin != tmin) {
            tmin = tzmin;
        }

        if (tzmax < tmax || tmax != tmax) {
            tmax = tzmax;
        }

        if (tmax < 0.0) {
            return create_vec3();
        }

        let max = if tmin >= 0.0 {
            tmin
        } else {
            tmax
        };

        return self.at(max, target);
    }
}