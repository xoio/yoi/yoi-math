# yoi-math

A set of common math helpers for Rust. 
Includes vector and matrix operations as well as other useful things. 

Also exports common functionality associated with the [noise](https://crates.io/crates/noise) crate.

This is mainly for personal use, but hopefully helpful to someone out there. 


